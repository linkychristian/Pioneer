import FRandom as random

from EntidyClass import *
from MathDef import *

SpeclEnt=0
def AIDef_init(speclent,ent):
    global SpeclEnt,Ent
    SpeclEnt=speclent
    Ent=ent
    Lizixiaoguo.AI=AIBind(ChaseAI,SpeclEnt)
#
def ride(f,peo):
    ent.force.x+=f.x
    ent.force.y+=f.y
    
def route(f,peo):
    peo.turn=f

def move(speed,ent):
    #speed=lis[0]#meter per gametick
    ent.force.x-=speed*math.sin(ent.face/radp)
    ent.force.y-=speed*math.cos(ent.face/radp)
#
def ChaseAI(self,player,e,speed=0.1):
    e[fl(self.x)][fl(self.y)].remove(self)
    self.face=180+math.atan2(player.x-self.x,player.y-self.y)*radp
    self.x=(1-speed)*self.x+speed*player.x
    self.y=(1-speed)*self.y+speed*player.y
    Addentidy(e,self)
def WalkAI(ch,sp):
    def Temp(self,player):
        if random.random()<ch:
            move(sp,self)
        else:
            route((random.random()-0.5)*30,self)
    return Temp
def AIBind(f,*args):
    def Temp(self,player):
        f(self,player,*args)
    return Temp
def RandomMove(self,player):
    if self.npctype==2:
        ChaseAI(self,player,Ent,-0.1)
        if (self.x-player.x)**2+(self.y-player.y)**2<=9:
            Addentidy(SpeclEnt,Lizixiaoguo(self.x,self.y,0))
        return
    try:
        self.cnt+=1
    except:
        self.cnt=0
    if self.cnt>10:
        self.cnt=0
        Addentidy(SpeclEnt,Lizixiaoguo((self.x+player.x)/2,(self.y+player.y)/2,1))
        Ent[fl(self.x)][fl(self.y)].remove(self)
        self.x=player.x+(random.random()-0.5)*25
        self.y=player.y+(random.random()-0.5)*25
        self.face=180+math.atan2(player.x-self.x,player.y-self.y)*radp
        Addentidy(Ent,self)
Pig.AI=WalkAI(0.875,0.07)
Treeman.AI=WalkAI(0.875,0.07)
Mouse.AI=WalkAI(1.0/6.0,0.35)
tmpNPC.AI=RandomMove

def PigDeath(self):
    if random.random()<0.5:
        Addentidy(SpeclEnt,Dropitem(fl(self.x)+random.random()*1.5-0.25,fl(self.y)+random.random()*1.5-0.25,Item(11,1)))

Pig.dead=PigDeath

def PeopleGetItem(self,dritem):
    for nm in (1,2,3,4,5,6,7,8,9,0):
        d=self.bag[nm]
        if not d.id:
            d.id=dritem.save.id
        if d.id==dritem.save.id:
            if d.cnt+dritem.save.cnt>ItemHeap[d.id]:
                dritem.save.cnt=d.cnt+dritem.save.cnt-ItemHeap[d.id]
                d.cnt=ItemHeap[d.id]
            else:
                d.cnt+=dritem.save.cnt
                dritem.save.cnt=0
                break
    if dritem.save.cnt==0:
        dritem.life=0
Steve.GetItem=PeopleGetItem