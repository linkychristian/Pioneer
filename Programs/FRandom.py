class _Random:
    def __init__(self):
        self._a=self._b=self._c=self._d=1
    def seed(self,x):
        x,self._a=divmod(x,30267)
        x,self._b=divmod(x,30305)
        x,self._c=divmod(x,30321)
        self._d=x%30365
    def _ranrg(self):
        self._a=(171*self._a)%30269
        self._b=(172*self._b)%30307
        self._c=(170*self._c)%30323
        self._d=(173*self._d)%30367
        return self._a*27901727585325L+self._b*920697165L+self._c*30365+self._d-27902648312856L
    def random(self):
        return self._ranrg()/844501588825031775.0
    def randrange(self,x):
        return int(self.random()*x)
    def randrange2(self,l,r):
        return l+int(self.random()*(r-l))
    def randint(self,l):
        return int(self.random()*(l-1))
    def randint2(self,l,r):
        return int(l+self.random()*(r-l+1))
_Inst=_Random()
seed=_Inst.seed
random=_Inst.random
randrange=_Inst.randrange
randrange2=_Inst.randrange2
randint=_Inst.randint
randint2=_Inst.randint2
