#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
long long seed;
int world_seed,biome[600][600],mineral[600][600];
int river[600][600],river_seed;
int conside[600][600],entidy[600][600];
int square_x,square_y;
int biome_catch;
int plant_born[310][2][2],plant[600][600];
short int hash_root[2][65][130000];
int Rand(long long zydx,long long zydy) {
	long long sd=(zydx*998776019LL+zydy*761824979LL+seed*379221113LL)%597279797LL;
	srand(sd);
	seed=sd;
	return rand();
}
struct Point {
	int x,y,col;
};
bool Check(int fz,int fm,int s_x,int s_y) {
	if(!fm&&!fz) return 0;
	seed=abs(1ll*world_seed*(1ll*fm*fz%998244353+1ll*114514*fm%998244353)%(1000000007));
	for(int i=1; i<=Rand(s_x,s_y)%8; i++) s_x=(s_x+Rand(s_x,s_y))%998244353,s_y=(s_y+Rand(s_x,s_y))%19260817; 
	fz=fz+Rand(s_x+fz,s_y)%fz;
	int rd=Rand(s_x,s_y+fm)%(fm+Rand(fz+s_x,fm+s_y)%fz);
	seed=world_seed;
	return rd<fz;
} 
void DownPut(int t1,int t2,int a1,int a2) {
	plant_born[t1][t2][0]=a1;
	plant_born[t1][t2][1]=a2;
}
void ResetPlantBorn() {
	//ֲ�����ɸ��� 0�ǹ�ľ 1����ľ 
	DownPut(1,0,3,1024);
	DownPut(2,0,4,256),DownPut(2,1,2,256);
	DownPut(3,0,30,256),DownPut(3,1,100,256);
	DownPut(5,0,5,2048),DownPut(5,1,3,2048);
	DownPut(7,0,3,512),DownPut(7,1,1,256);
	DownPut(9,0,5,256),DownPut(9,1,10,256);
	DownPut(10,0,15,512),DownPut(10,1,15,512);
	DownPut(12,0,5,512),DownPut(12,1,1,256);
	DownPut(103,0,60,1024),DownPut(103,1,15,1024);
}
bool operator<(Point a1,Point a2) {
	return a1.x==a2.x?(a1.y==a2.y?a1.col<a2.col:a1.y<a2.y):(a1.x<a2.x);
}
const double P = 0.50 ;
double a[700][700] , b[700][700] ;
ll c[700][700] ;
int cross[25][2]= {{0,0},{0,1},{0,-1},{1,0},{-1,0},{-1,-1},{-1,1},{1,-1},{1,1},
                   {-2,-2},{-2,-1},{-2,0},{-2,1},{-2,2},{-1,-2},{-1,2},
				   {0,-2},{0,2},{1,-2},{1,2},{2,-2},{2,-1},{2,0},{2,1},{2,2}};
inline int TY(int num) {
	return num-square_y;
}
inline int TX(int num) {
	return num-square_x;
}
inline void output(int num) {
	putchar(num>>8);
	putchar(num&0xff);
}

inline void input(int& num) {
	int c1=getchar(),c2=getchar();
	num=(c1<<8)+c2;
}
void Special_Create(Point& tmp,int type) {
	seed=world_seed;
	int roll=Rand(tmp.x,tmp.y)%1000;
	if(type==1) {//�������Լ�����
		if(roll<20 || (tmp.col==12 && roll<50)) {
			if(tmp.col==3&&(Rand(tmp.x,tmp.y)%100<15)) tmp.col=102;
			else tmp.col=17;
		}
	}
	if(type==2) {//ѩԭ��Ҷ��
		if(roll<100&&tmp.col==5) tmp.col=103;
	}
	if(type==3) {//�ҽ���
		if(((roll<4)||
		        (tmp.col==6&&roll<20))||
		        (tmp.col==1&&roll<10)) tmp.col=104;
	}
	if(type==4) {//���͵���
		if(roll<10) tmp.col=105;
	}
	if(type==5) {//С�͵���
		if(roll<2) tmp.col=105;
	}
	if(type==7) {//�����ˮ�� 
		if(roll<15) tmp.col=17; 
	}
}
int HashPos(int xx,int yy,int len) {
	if(!xx&&!yy) return 320+1+102400;
	if(!xx) return 320+yy/len+1+102400;
	if(!yy) return (xx/len+1)*320+1+102400;
    return (xx/len+1)*320+yy/len+1+102400;
}	
Point GetPoint(int s_x,int s_y,int len);
void UnrealLoad(int s_x,int s_y,Point& now,int len) {
	len<<=2;
	int min_len=998244353;
	Point min_close;
	s_x=s_x/len*len,s_y=s_y/len*len;
	for(int i=0; i<9; i++) {
		if(s_x+cross[i][0]*len<0||s_y+cross[i][1]*len<0) continue;
		Point tmp=GetPoint(s_x+cross[i][0]*len,s_y+cross[i][1]*len,len);
		ll far=pow(abs(tmp.x-now.x),2)+pow(abs(tmp.y-now.y),2);
		if(far<min_len) min_len=far,min_close=tmp;
	}
	now.col=min_close.col;
}
Point GetPoint(int s_x,int s_y,int len) {
	seed=world_seed;
	int t_pos=HashPos(TX(s_x),TY(s_y),len);
	if(hash_root[0][len>>5][t_pos]) return (Point) {s_x+Rand(s_x,s_y)%len,s_y+Rand(s_x,s_y)%len,hash_root[0][len>>5][t_pos]};
	Point tmp;
	tmp.x=s_x+Rand(s_x,s_y)%len,tmp.y=s_y+Rand(s_x,s_y)%len;
	if(len==biome_catch) tmp.col=Rand(tmp.x,tmp.y)%13-13;
	else {
		UnrealLoad(s_x,s_y,tmp,len);
		seed=world_seed,Rand(s_x,s_y),Rand(s_x,s_y);
		if(tmp.col<-8) tmp.col=17;
		else if(tmp.col>=-8&&tmp.col<0) tmp.col=Rand(tmp.x,tmp.y)%16+1;
		if(len==32) {
			if(tmp.col!=17) Special_Create(tmp,Rand(tmp.x,tmp.y)%3+1);
			else Special_Create(tmp,4);
		}
		if(len==8) {
			if(tmp.col==12) Special_Create(tmp,7); 
			else if(tmp.col==17) Special_Create(tmp,5);
		}
	}
	hash_root[0][len>>5][t_pos]=tmp.col;
	seed=world_seed;
	return tmp;
}
void SquareMade(int s_x,int s_y,int len) {
	seed=world_seed;
	Point root=GetPoint(s_x,s_y,len);
	vector<Point> root_queue;
	int flag=true;
	root_queue.push_back(root);
	for(int i=1; i<=8; i++) {
//		if(cross[i][0]&&cross[i][1]) continue;
		if(s_x+cross[i][0]*len<0||s_y+cross[i][1]*len<0) continue;
		Point tmp=GetPoint(s_x+cross[i][0]*len,s_y+cross[i][1]*len,len);
		if(tmp.col!=root.col) flag=0;
		root_queue.push_back(tmp);
	}
//	cout<<root_queue.size()<<endl;
	if(flag) {
		for(int i=s_x; i<s_x+len; i++)
			for(int j=s_y; j<s_y+len; j++) biome[TX(i)][TY(j)]=root.col;
	} else {
		for(int i=s_x; i<s_x+len; i++) {
		    for(int j=s_y; j<s_y+len; j++) {
			    int min_len=998244353;//:)
			    Point min_close;
			    for(int k=0; k<root_queue.size(); k++) {
					Point tmp=root_queue[k];
					ll far=pow(abs(tmp.x-i),2)+pow(abs(tmp.y-j),2);
					if(far<min_len) min_len=far,min_close=tmp;
				}
				biome[TX(i)][TY(j)]=min_close.col;
			}
		}
	}
	if(len==8&&root.col==12) {
		int water_pool_num=Rand(s_x,s_y)%23;
		while(water_pool_num--) {
			int x=s_x+Rand(s_x,s_y)%len,y=s_y+Rand(s_x,s_y)%len;
			if(biome[TX(x)][TY(y)]==12) biome[TX(x)][TY(y)]=17;
		}
	}
	seed=world_seed;
}
Point RiverPoint(int s_x,int s_y,int len);

void UnrealLoadRiver(int s_x,int s_y,Point& now,int len) {
	len*=4;
	ll min_len=1e12;
	Point min_close;
	s_x=s_x/len*len,s_y=s_y/len*len;
	for(int i=0; i<9; i++) {
		if(s_x+cross[i][0]*len<0||s_y+cross[i][1]*len<0) continue;
		Point tmp=RiverPoint(s_x+cross[i][0]*len,s_y+cross[i][1]*len,len);
		ll far=pow(abs(tmp.x-now.x),2)+pow(abs(tmp.y-now.y),2);
		if(far<min_len) min_len=far,min_close=tmp;
	}
	now.col=min_close.col;
}
Point RiverPoint(int s_x,int s_y,int len) {
	seed=river_seed;
	int t_pos=HashPos(TX(s_x),TY(s_y),len);
	if(hash_root[1][len>>5][t_pos]) return (Point) {s_x+Rand(s_x,s_y)%len,s_y+Rand(s_x,s_y)%len,hash_root[1][len>>5][t_pos]};
	Point tmp;
	tmp.x=s_x+Rand(s_x,s_y)%len,tmp.y=s_y+Rand(s_x,s_y)%len;
	if(len==512) tmp.col=Rand(tmp.x,tmp.y)%3+1;
	else UnrealLoadRiver(s_x,s_y,tmp,len);
    hash_root[1][len>>5][t_pos]=tmp.col;
	seed=river_seed;
	return tmp;
}
void RiverMade(int s_x,int s_y,int len) {
	seed=river_seed;
	Point root=RiverPoint(s_x,s_y,len);
	vector<Point> root_queue;
	root_queue.push_back(root);
	for(int i=1; i<=8; i++) {
		if(s_x+cross[i][0]*len<0||s_y+cross[i][1]*len<0) continue;
		Point tmp=RiverPoint(s_x+cross[i][0]*len,s_y+cross[i][1]*len,len);
		root_queue.push_back(tmp);
	}
	for(int i=s_x; i<s_x+len; i++) {
		for(int j=s_y; j<s_y+len; j++) {
			int min_len=998244353;//:)
			Point min_close;
			for(int k=0; k<root_queue.size(); k++) {
				Point tmp=root_queue[k];
				ll far=pow(abs(tmp.x-i),2)+pow(abs(tmp.y-j),2);
				if(far<min_len) min_len=far,min_close=tmp;
			}
			river[TX(i)][TY(j)]=min_close.col;
		}
	}
}
void Turn2River(int s_x,int s_y,int len) {
	for(int i=s_x; i<s_x+len; i++) {
		for(int j=s_y; j<s_y+len; j++) {
			for(int k=0; k<25; k++) {
				int tx=i+cross[k][0],ty=j+cross[k][1];
				if(tx<0||ty<0||conside[TX(i)][TY(j)]) continue;
				if(tx<square_x||tx>=square_x+512
				||ty<square_y||ty>=square_y+512) continue;
				if(river[TX(tx)][TY(ty)]!=river[TX(i)][TY(j)]) 
				    conside[TX(i)][TY(j)]=1,
				    conside[TX(tx)][TY(ty)]=1;
			}
		}
	}
}
void MakePlant(int s_x,int s_y,int len) {
	for(int i=s_x; i<s_x+len; i++) {
		for(int j=s_y; j<s_y+len; j++) {
			int biome_here=biome[TX(i)][TY(j)];
			if(Check(plant_born[biome_here][0][0],plant_born[biome_here][0][1],i,j)) plant[TX(i)][TY(j)]=1003;
			if(Check(plant_born[biome_here][1][0],plant_born[biome_here][1][1],i,j)) plant[TX(i)][TY(j)]=1001;
		}
	}
}
//int qpow(int a1,int a2) {
//	int res=1;
//	while(a2) {
//		if(a2&1) res*=a1;
//		a1*=a1;
//		a2>>=1; 
//	}
//	return res;
//} 
//double hash21 (int x , int y) {
//	int n = x+y*57 ;
//	n=(n<<13)^n ;
//    return (1.0 - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0) ;
//}
//double Smooth (int x , int y) {
//	double t1 = (hash21 (x - 1 , y - 1) + hash21 (x - 1 , y + 1) + hash21 (x + 1 , y - 1) + hash21 (x + 1 , y + 1)) / 16 ;
//	double t2 = (hash21 (x , y - 1) + hash21 (x , y + 1) + hash21 (x - 1 , y) + hash21 (x + 1 , y)) / 8 ;
//	double t3 = hash21 (x , y) / 4 ;
//	return t1 + t2 + t3 ;
//}
//double FuncNoise(double t) {
//	return cos(t);
//}
//double InsCos (double x , double y , double p) {
//	double t = p * 3.1415927;
//	double f = (1 - FuncNoise(t)) * 0.5 ;
//	return x * (1 - f)+y*f;
//}
//double ins (double x , double y) {
//	int X = int (x) , Y = int (y) ;
//	double xx = X - x , yy = Y - y ;
//	double v1 = Smooth (X , Y) , v2 = Smooth (X + 1 , Y) , v3 = Smooth (X , Y + 1) , v4 = Smooth (X + 1 , Y + 1) ;
//	if (x<0) {
//		double i1 = InsCos (v2 , v1 , xx) , i2 = InsCos (v4 , v3 , xx) ;
//		return y < 0 ? InsCos (i2 , i1 , yy) : InsCos (i1 , i2 , yy) ;
//	}
//	else {
//		double i1 =InsCos(v1 , v2 , xx) , i2 = InsCos (v3 , v4 , xx) ;
//		return y >= 0 ? InsCos (i1 , i2 , yy) : InsCos (i2 , i1 , yy) ;
//	}
//}
//double ValueNoise (double x , double y) {
//	double tot = 0 ;
//	for (int i = 0 ;i<8; i++) {
//		double t1 = qpow(2,i) , t2 = qpow(0.4,i);
//		tot += (ins(x*t1,y*t1)*t2) ;
//	}
//	return tot ;
//}
//void make_noise(int s_x,int s_y,int len) {
//	int X=square_x,Y=square_y;
//	for (int i = s_x ; i < s_x+len ; i++)
//		for (int j = s_y ; j < s_y+len ; j++)
//			a[TX(i)][TY(j)] = ValueNoise (i / 250.0 + X * 1.0 , j / 250.0 + Y * 1.0) ;
//	for (int i = s_x ; i < s_x+len ; i++)
//		for (int j = s_y ; j < s_y+len ; j++)
//			c[TX(i)][TY(j)]=floor((a[TX(i)][TY(j)] + 1.0) * 0.5 * 250) ;
//}
int main() {
	freopen("Map/SMP.in","r",stdin);
	freopen("Map/Cache","w",stdout);
	scanf("%d%d%d",&square_x,&square_y,&world_seed);
//    square_x=0,square_y=0,world_seed=3;
	biome_catch=2048;
	river_seed=fabs(world_seed+Rand(1245,4927)-Rand(1245,4927));
	seed=world_seed;
	for(int i=0; i<512; i+=8)
		for(int j=0; j<512; j+=8) {
			SquareMade(square_x+i,square_y+j,8);
			RiverMade(square_x+i,square_y+j,8);
		}
	Turn2River(square_x,square_y,512);
	ResetPlantBorn(),MakePlant(square_x,square_y,512);
//	make_noise(square_x,square_y,biome_catch);
	for(int i=0; i<512; i++) {
		for(int j=0; j<512; j++) {
			if(conside[i][j]) output(17);
//			else if(c[i][j]/1.28<=105) output(17);
//			else if(c[i][j]/1.28<=108) output(1);
//			else if(plant[i][j]&&biome[i][j]!=17) output(plant[i][j]); 
			else output(biome[i][j]);
		}
	}
	for(int i=0; i<512; i++) 
		for(int j=0; j<512; j++) {
			if(conside[i][j]) output(0);
			else output(plant[i][j]);
		}
//    system("MapOutput.exe");
	return 0;
}

