def Makerect(pls):
    for i in xrange(len(pls)):
        pls[i]._GEnergyimg=pygame.transform.scale(pls[i]._GEnergyimg,(32,32))
        pls[i]._GEnergyrect=(i*100,0,i*100+32,32)#LURD

def executeGame():
    global typen,chn,chc
    typen=0
    return False

def init(pg):
    global pygame,paiimg
    pygame=pg
    paiimg=[]
    for i in xrange(17):
        paiimg.append(pygame.transform.scale2x( \
                      pygame.transform.scale2x( \
                      pygame.image.load("Datas/Pai/%d.bmp" % (i,)) \
                      )))
def main(pls):
    global typen,chn,chc,screen
    flag=True
    typen=0
    chn=0
    chc=0
    Makerect(pls)
    screen=pygame.display.set_mode((800,800))
    while flag:
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                flag=False
            elif event.type==pygame.MOUSEBUTTONUP:
                if typen==0 and 700<=event.pos[1]<764 and 128<=event.pos[0]<672:
                    chc=(event.pos[0]-128)/32
                    typen=1
                elif typen==1:
                    for i in xrange(1,len(pls)):
                        if pls[i]._GEnergyrect[0]<=event.pos[0]<pls[i]._GEnergyrect[2] and \
                           pls[i]._GEnergyrect[1]<=event.pos[1]<pls[i]._GEnergyrect[3]:
                            typen=2
                            chn=pls[i]
        if typen==2:
            if executeGame():
                flag=0
        mx,my=pygame.mouse.get_pos()
        screen.fill((127,127,127))
        for i in xrange(len(pls)):
            screen.blit(pls[i]._GEnergyimg,pls[i]._GEnergyrect[:2])
            if i and pls[i]._GEnergyrect[0]<=mx<pls[i]._GEnergyrect[2] and \
                     pls[i]._GEnergyrect[1]<=my<pls[i]._GEnergyrect[3]:
                sc=pygame.Surface((pls[i]._GEnergyrect[2]-pls[i]._GEnergyrect[0],pls[i]._GEnergyrect[3]-pls[i]._GEnergyrect[1]))
                sc.fill((255,255,255))
                sc.set_alpha(127)
                screen.blit(sc,(pls[i]._GEnergyrect[0],pls[i]._GEnergyrect[1]))
        if typen==0:
            for i in xrange(17):
                screen.blit(paiimg[i],(128+i*32,700))
            if 700<=my<764 and 128<=mx<672:
                sc=pygame.Surface((32,64))
                sc.fill((255,255,255))
                sc.set_alpha(127)
                screen.blit(sc,(mx/32*32,700))
        pygame.display.flip()
